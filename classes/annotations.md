Rooms
-----

A room must have:

- at least a door


Doors
-----

Not exactly a door. It can be a corridor, path, etc. A door holds information about the other doors it can connect to.

- Position
- Possible connections


Placing of rooms
-----

Rooms can't overlap. The initial idea is to use a grid and mark occupied cells and check
before placing a new room.

When I figure out how to place rooms I can try to rotate rooms. The doors then will have information about which rooms they can connect based on the rotation on the room (0, 90, 180, 270)

example:
```
class Door
    connections = {
        0: [id1, id2, id4...],
        90: [id3, id7, id12],
        180: ...
    }
```

**but focus on making it work first!!!**

-----

How to store data?
-----

I'm planning to store jsons with the data and references to scenes. Like

```
Collection
|   rooms
|   doors
|   npcs
|   objects

Room
|   size (in cell space)
|   position of doors
|   positions of npcs (?)
|   positions of objects

Door
|   which ones it can connect with
```

When creating a stage, it will be fed with collections.

Feed with a room collection. 

Stage
-----

Receives rooms collections, npcs, objs, etc.

in the stage editor we will
- set the rooms that will be used on that stage
- - rooms that must be used
- - rooms that can be used only once
- - etc
- set the enemies
- - level range
- - drops
- - enemies that spawn once
- - etc

**but initially only the placement of rooms: only a collection and number of rooms will be given**

Editor
---

(collection of rooms)
1. We import the images/meshes (okay-ish)
2. We create the doors
    - receives a color for visual feedback
    - add connection to other doors
3. We put doors on the rooms

Then we try to create a stage

1. pick a random room and place it
2. pick a random door of that room and then:
    - check the rooms that can connect to that door
    - pick a random one and see if it fits. if it doesn't, try another
    - if there are no rooms that can fit, go back to the previous room and place a different one or something
3. repeat 2 until stage is made
4. draw the stage

-----

Room collection structure
-----

```json
collection = {
    "rooms": {
        // id: { texture: filepath, doors: [], objs: [], ...}
        "1": {
            "texture": "path/to/file",
            "doors": [
                {"id": 1, "x": 84, "y": 22},
                {"id": 3, "x": 40, "y": 55}
            ],
            "objs": []
        }
    },
    "doors": {
        // id: connections[]
        "1": [2, 4],
        "2": [3, 5]
    }
}
```

