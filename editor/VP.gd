extends Node2D

onready var camera = $Camera
onready var sprite = $Sprite

var panning := false
var zoom_factor := 1.0
var zoom_amount := 0.1

func _ready() -> void:
	set_process_unhandled_input(true)
	set_process_input(true)


func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		if panning:
			camera.position -= event.relative * zoom_factor
	
func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_MIDDLE:
			panning = event.pressed
		
		elif event.button_index == BUTTON_WHEEL_UP and event.pressed:
			zoom_at_point(-zoom_amount, camera.get_local_mouse_position())
		elif event.button_index == BUTTON_WHEEL_DOWN and event.pressed:
			zoom_at_point(zoom_amount, camera.get_local_mouse_position())


func zoom_at_point(zoom_change: float, point: Vector2) -> void:
	var f0 = zoom_factor
	var f1 = clamp(zoom_factor + zoom_change, 0.1, 10)
	if f0 != f1:
		zoom_factor = f1
		camera.zoom = Vector2(zoom_factor, zoom_factor)
		camera.position -= point * zoom_change


func set_zoom(zoom: float) -> void:
	zoom_factor = zoom
	camera.zoom = Vector2(zoom_factor, zoom_factor)

func set_camera_pos(pos: Vector2) -> void:
	camera.position = pos


# called from RoomEditor.gd (RoomList item activated)
func set_sprite(texture: Texture) -> void:
	sprite.texture = texture

